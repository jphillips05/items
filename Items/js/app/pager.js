﻿(function() {
    angular
        .module('app')
        .directive('pager', pager);

    pager.$inject = [];
    function pager() {

        controller.$inject = ['$scope'];
        function controller($scope) {

            $scope.pages = [];
            $scope.pageID = 0;

            $scope.updatePage = updatePage;

            activate();
            function activate() {
            }

            function updatePage(pageID) {
                $scope.pageID = pageID;
                var page = $scope.pages.filter(function(page) { return page.id == pageID; })[0];

                $scope.page = page.id;
                $scope.take = page.take;
                $scope.skip = page.skip;
                $scope.$emit('page');
            }

            $scope.$watch('count', function(val) {
                if (val > $scope.take) {
                    var skip = $scope.skip !== undefined ? $scope.skip : 0;

                    for (i = 0; i < Math.ceil($scope.count / $scope.take) ; i++) {
                        $scope.pages.push({
                            id: i,
                            skip: skip,
                            take: $scope.take
                        });

                        skip = skip + $scope.take;
                    }
                }
            });

        }

        return {
            controller: controller,
            scope: {
                count: '=',
                skip: '=',
                take: '='
            },
            templateUrl: 'js/app/pager.html'
        };

    }

})();