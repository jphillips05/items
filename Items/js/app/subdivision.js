﻿(function() {
    angular
        .module('app')
        .factory('Subdivision', Subdivision);

    Subdivision.$inject = ['$http', '$q'];
    function Subdivision($http, $q) {
        var defer = $q.defer();

        function xhrSuccess(res) {
            return res.data;
        }

        function xhrError(err) {
            return defer.reject(err);
        }

        return {
            list: function(DivisionID) {
                return $http({
                    url: 'api/Subdivision',
                    method: 'GET',
                    params: {
                        DivisionID: DivisionID
                    }
                })
                .then(xhrSuccess)
                .catch(xhrError);
            }
        };

    }

})();