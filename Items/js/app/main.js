﻿(function() {
    angular
        .module('app')
        .directive('main', main);

    main.$inject = [];
    function main() {
        controller.$inject = ['$scope', 'Division', 'Subdivision', 'Item'];
        function controller($scope, Division, Subdivision, Item) {
            
            $scope.take = 500;
            $scope.skip = 0;
            $scope.count = 0;

            $scope.selectDivision = selectDivision;
            $scope.selectSubdivision = selectSubdivision;
            $scope.updateItemsubdivision = updateItemsubdivision;
            $scope.getItems = getItems;
            $scope.updateItem = updateItem;

            activate();
            function activate() {
                Division.list().then(function(data) {
                    $scope.divisions = data;
                });
            }

            function selectDivision(division) {
                $scope.divisionID = division.DivisionID;
                $scope.subdivisionID = 0;
                Subdivision.list(division.DivisionID).then(function(data) {
                    $scope.subdivisions = data;
                });
            }

            function selectSubdivision(subdivision) {
                $scope.items = [];
                $scope.subdivisionID = subdivision.SubDivisionID;
                $scope.getItems();
            }

            function getItems() {
                $scope.items = [];

                Item.list($scope.subdivisionID, $scope.take, $scope.skip || 0).then(function(data) {
                    $scope.items = data.Table1;
                    $scope.count = data.Table2[0].Count;
                });
            }

            function updateItemsubdivision(ItemID, SubDivisionID) {
                console.log(ItemID, SubDivisionID);
                if (confirm('Are you sure you want to change this SubDivision?')) {
                    Item.updateSubDivision(ItemID, SubDivisionID).then(function(data) {
                        console.log(data);
                    });
                }
            }

            function updateItem(ItemID, item) {
                Item.updateItem(ItemID, item).then(function(data) {
                    console.log(data);
                });
            }

            $scope.$on('page', function() {
                $scope.getItems();
            });

        }

        return {
            controller: controller,
            templateUrl: 'js/app/main.html'
        };

    }

})();