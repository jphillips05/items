﻿(function() {
    angular
        .module('app')
        .factory('Division', Division);

    Division.$inject = ['$http','$q'];
    function Division($http, $q) {
        var defer = $q.defer();

        function xhrSuccess(res) {
            return res.data;
        }

        function xhrError(err) {
            return defer.reject(err);
        }

        return {
            list: function() {
                return $http({
                    url: 'api/Division',
                    method: 'GET'
                })
                .then(xhrSuccess)
                .catch(xhrError);
            }
        };

    }

})();