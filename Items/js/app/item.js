﻿(function() {
    angular
        .module('app')
        .factory('Item', Item);

    Item.$inject = ['$http', '$q'];
    function Item($http, $q) {
        var defer = $q.defer();

        function xhrSuccess(res) {
            return res.data;
        }

        function xhrError(err) {
            return defer.reject(err);
        }

        return {
            list: function(DivisionID) {
                return $http({
                    url: 'api/Item',
                    method: 'GET',
                    params: {
                        DivisionID: DivisionID
                    }
                })
                .then(xhrSuccess)
                .catch(xhrError);
            },

            updateSubDivision: function(ItemID, SubDivisionID) {
                return $http({
                    url: 'api/Item/' + ItemID + '/SubDivision',
                    method: 'POST',
                    params: {
                        SubDivisionID: SubDivisionID
                    }
                })
                .then(xhrSuccess)
                .catch(xhrError);
            },

            updateItem: function(ItemID, item) {
                return $http({
                    url: 'api/Item/' + ItemID,
                    method: 'POST',
                    data: item
                })
                .then(xhrSuccess)
                .catch(xhrError);
            }
        };

    }

})();