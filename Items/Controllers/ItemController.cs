﻿using Items.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Items.Controllers
{
    [RoutePrefix("api/Item")]
    public class ItemController : ApiController
    {
        private Item item { get; set; }
        private ItemController()
        {
            item = new Item();
        }

        [Route("")]
        public IHttpActionResult Get(int DivisionID, int skip = 0, int take = 500)
        {
            return Ok(item.List(DivisionID, skip, take));
        }

        [Route("{ItemID:int}/SubDivision")]
        public IHttpActionResult UpdateSubdivision(int ItemID, int SubDivisionID)
        {
            return Ok(new { success = item.UpdateSubdivision(ItemID, SubDivisionID) });
        }

        [Route("{ItemID:int}")]
        public IHttpActionResult UpdateItem(int ItemID, Item item)
        {
            return Ok(new { success = item.UpdateItem(ItemID, item) });
        }

    }
}
