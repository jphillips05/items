﻿using Items.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Items.Controllers
{
    [RoutePrefix("api/Subdivision")]
    public class SubdivisionController : ApiController
    {
        private Subdivision subdivision { get; set; }

        private SubdivisionController()
        {
            subdivision = new Subdivision();
        }

        [Route("")]
        public IHttpActionResult Get(int DivisionID)
        {
            return Ok(subdivision.List(DivisionID));
        }
    }
}
