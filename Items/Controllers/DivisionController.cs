﻿using Items.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Items.Controllers
{
    [RoutePrefix("api/Division")]
    public class DivisionController : ApiController
    {
        private Division division { get; set; }
        public DivisionController()
        {
            division = new Division();
        }

        [Route("")]
        public IHttpActionResult Get()
        {
            return Ok(division.List());
        }

    }
}
