﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Items.Models
{
    public class Division
    {
        private SQL sql { get; set; }

        public Division() {
            sql = new SQL();
        }

        public DataTable List()
        {
            DataTable dt = new DataTable();

            try
            {
                sql.Parameters.Clear();
                dt = sql.ExecuteDT("Select * from vDivision");
            }
            catch (Exception ex)
            {
            }

            return dt;
        }
    }
}