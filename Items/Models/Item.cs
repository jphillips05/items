﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Items.Models
{
    public class Item
    {
        private SQL sql { get; set; }
        public int ItemID { get; set; }
        public string ItemDescription { get; set; }
        public string Misc { get; set; }
        public string Misc2 { get; set; }
        public string Misc3 { get; set; }
        public string Misc4 { get; set; }

        public Item()
        {
            sql = new SQL();
        }

        public DataSet List(int SubDivisionID, int skip, int take)
        {
            DataSet ds = new DataSet();
            DataTable dtItems = new DataTable("Items");
            DataTable dtCount = new DataTable("count");

            sql.Parameters.Clear();
            sql.Parameters.Add("@SubDivisionID", SubDivisionID);
            sql.Parameters.Add("@skip", skip);
            sql.Parameters.Add("@take", take);

            dtItems = sql.ExecuteDT(@"
                    with items as (
                        Select *, ROW_NUMBER() OVER (ORDER BY ItemID) AS 'RowNumber'
                        from vItemsCombined 
                        where SubDivisionID = @SubDivisionID
                    )
                    select * from items where RowNumber between @skip and @take                    
                    ");

            sql.Parameters.Clear();
            sql.Parameters.Add("@SubDivisionID", SubDivisionID);
            dtCount = sql.ExecuteDT("select count(*) as Count from vItemsCombined where SubDivisionID = @SubDivisionID ");

            ds.Tables.Add(dtItems);
            ds.Tables.Add(dtCount);

            return ds;
        }

        public bool UpdateSubdivision(int ItemID, int SubDivsionID)
        {
            sql.Parameters.Clear();
            sql.Parameters.Add("@ItemID", ItemID);
            sql.Parameters.Add("@SubDivisionID", SubDivsionID);

            if (sql.Execute("Update temp_Item set SubDivisionID = @SubDivisionID where ItemID = @ItemID") > 0)
            {
                return true;
            }

            return false;

        }

        public bool UpdateItem(int ItemID, Item item)
        {
            sql.Parameters.Clear();
            sql.Parameters.Add("@ItemID", ItemID);
            sql.Parameters.Add("@Description", item.ItemDescription);
            bool ItemQuery = sql.Execute("Update temp_Item set Description = @Description where ItemID = @ItemID") > 0;

            sql.Parameters.Clear();
            sql.Parameters.Add("@ItemID", ItemID);
            sql.Parameters.Add("@Misc", item.Misc);
            sql.Parameters.Add("@Misc2", item.Misc2);
            sql.Parameters.Add("@Misc3", item.Misc3);
            sql.Parameters.Add("@Misc4", item.Misc4);
            bool ItemDetailQuery = sql.Execute(@"
                Update temp_ItemDetail 
                set 
                    Misc = @Misc,
                    Misc2 = @Misc2, 
                    Misc3 = @Misc3,
                    Misc4 = @Misc4
                where ItemID = @ItemID") > 0;

            if (ItemQuery && ItemDetailQuery)
            {
                return true;
            }

            return false;
        }
    }
}