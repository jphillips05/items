﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Items.Models
{
    public class Subdivision
    {
        private SQL sql { get; set; }

        public Subdivision() {
            sql = new SQL();
        }

        public DataTable List(int DivisionID)
        {
            DataTable dt = new DataTable();

            try
            {
                sql.Parameters.Clear();
                sql.Parameters.Add("@DivisionID", DivisionID);
                dt = sql.ExecuteDT("select * from vSubdivision where DivisionID = @DivisionID order by SubDivisionID");
            }
            catch (Exception ex)
            {
            }

            return dt;
        }
    }
}