﻿var gulp = require('gulp');
var concat = require('gulp-concat');
var header = require('gulp-header');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var browserSync = require('browser-sync');
var pkg = require('./package.json');

var banner = ['/*!\n',
    ' * <%= pkg.title %> v<%= pkg.version %> \n',
    ' * Copyright 2016-' + (new Date()).getFullYear(), ' <%= pkg.author %>\n',
    ' */\n',
    ''
].join('');

gulp.task('copy-vendor', function() {
    gulp.src([
            'node_modules/angular/angular.js',
            'node_modules/angular-filter/dist/angular-filter.js'
    ])
        .pipe(concat('vendor'))
        .pipe(uglify())
        .pipe(header(banner, { pkg: pkg }))
        .pipe(rename({ suffix: '.min.js' }))
        .pipe(gulp.dest('js'));
});

gulp.task('copy-app', function() {
    gulp.src(['js/app/app.js', 'js/app/**/*.js'])
        .pipe(concat('app'))
        .pipe(uglify())
        .pipe(header(banner, { pkg: pkg }))
        .pipe(rename({ suffix: '.min.js' }))
        .pipe(gulp.dest('js'));
})

//jshint
gulp.task('hint-js', function() {
    return gulp.src(['js/app/app.js', 'js/app/**/*.js'])
        .pipe(jshint({}))
        .pipe(jshint.reporter(stylish))
});

// Run everything
gulp.task('default', ['copy-vendor', 'hint-js', 'copy-app']);

// Configure the browserSync task
gulp.task('browserSync', function() {
    browserSync.init({
        proxy: 'localhost:57939'
    })
})

// Dev task with browserSync
gulp.task('dev', ['browserSync', 'copy-vendor', 'hint-js', 'copy-app'], function() {
    gulp.watch(['js/app/**/*.js'], ['copy-app']);
    // Reloads the browser whenever HTML or JS files change
    gulp.watch(['**/*.html'], browserSync.reload);
    gulp.watch(['js/*.min.js'], browserSync.reload);
});